# TP2 : Network low-level, Switching

# Sommaire

* [Intro](#intro)
* [0. Etapes préliminaires](#0-etapes-préliminaires)
* [I. Simplest setup](#i-simplest-setup)
  * [Topologie](#topologie)
  * [Plan d'adressage](#plan-dadressage)
  * [ToDo](#todo)
* [II. More switches](#ii-more-switches)
  * [Topologie](#topologie-1)
  * [Plan d'adressage](#plan-dadressage-1)
  * [ToDo](#todo-1)
  * [Mise en évidence du Spanning Tree Protocol](#mise-en-évidence-du-spanning-tree-protocol)
* [III. Isolation](#iii-isolation)
  * [1. Simple](#1-simple)
    * [Topologie](#topologie-2)
    * [Plan d'adressage](#plan-dadressage-2)
    * [ToDo](#todo-2)
  * [2. Avec trunk](#2-avec-trunk)
    * [Topologie](#topologie-3)
    * [Plan d'adressage](#plan-dadressage-3)
    * [ToDo](#todo-3)
* [IV. Need perfs](#iv-need-perfs)
  * [Topologie](#topologie-4)
  * [Plan d'adressage](#plan-dadressage-4)
  * [ToDo](#todo-4)

# Intro

Dans ce TP on va se pencher un peu plus sur les échanges réseau en eux-mêmes, en **analysant les trames réseau avec Wireshark**. 

On va aussi jouer de façon un peu plus avancée avec des **switches**.

On va commencer à rentrer plus dans le détails des différents éléments.  
**Allez à votre rythme, prenez le temps de comprendre.**  
**Posez des questions.**  
**Prenez des notes au fur et à mesure.**  
**Lisez les parties en entier avant de commencer à travailler dessus.**

Pour ce qui est de la mise en place, on va manipuler des switches (IOS Cisco) et aborder les notions/protocoles suivants : 
* ARP
* `ping`
* Spanning-Tree : STP
* Utilisation de VLAN : Trunking
* Agrégation de ports : LACP

> **Référez-vous [au README des TPs](/tp/README.md) pour des infos sur le déroulement et le rendu des TPs.**

# 0. Etapes préliminaires

* avoir lu [le README des TPs](/tp/README.md)
* [**GNS3** installé et configuré](/memo/setup-gns3.md) (avec la **GNS3VM**, dans la même version)
* **Wireshark** installé
* Lecture du [mémo/setup GNS3](/memo/setup-gns3.md)
* Lecture du [mémo CLI Cisco](/memo/cli-cisco.md) section [Général](/memo/cli-cisco.md#general) et [Switches](/memo/cli-cisco.md#switches)

**Dans ce TP, vous pouvez considérez que :**
* les `PC` sont [**des VPCS de GNS3**](/memo/setup-gns3.md#utilisation-dun-vpcs) (sauf indication contraire)
* les `SW` sont des Switches Cisco, virtualisé avec l'IOU L2

# I. Simplest setup

#### Topologie

```
+-----+        +-------+        +-----+
| PC1 +--------+  SW1  +--------+ PC2 |
+-----+        +-------+        +-----+
```

#### Plan d'adressage

Machine | `net1`
--- | ---
`PC1` | `10.2.1.1/24`
`PC2` | `10.2.1.2/24`

#### ToDo

> **Si vous lancez Wireshark, et que vous mettez des dumps/captures d'écran, précisez où vous avez lancé Wireshark (sur quel lien réseau/quelle machine et quelle interface)**

* 🌞 mettre en place la topologie ci-dessus

![Topologie I](./images/TopologieI.png)

* 🌞 faire communiquer les deux PCs

Wireshark lancé sur la liaison entre PC1 et IOU1 pour le ping entre PC1 et PC2.

On peut voir que des lignes 54 à 57 on a l'ARP broadcast de PC1 et des lignes 58 à 69 on a l'échange de ping de PC1 à PC2.
![Wireshark Ping I](./images/WiresharkPingI.png)

On peut voir ici les échanges de ping de PC2 à PC1.
![Wireshark Ping I-2](./images/WiresharkPingI-2.png)

  On peut voir ci-dessus le protocole ARP utilisé par les deux PCs pour communiquer

* 🌞 expliquer...
  * pourquoi le switch n'a pas besoin d'IP:

    Le switch n'a pas besoin d'IP parce que l'ARP broadcast communique avec lui via son adresse MAC et non son adresse IP.
    Les seules adresses IPs qui sont communiquées dans la requète ARP sont les IPs du destinataire et de l'émetteur.

  * pourquoi les machines ont besoin d'une IP pour pouvoir se `ping`

    Le ping utilisant le protocole ICMP qui est lui-mème encapsulé dans un paquet IP, les machines ont besoin de connaître l'IP de destination pour communiquer

# II. More switches

#### Topologie

```
                        +-----+
                        | PC2 |
                        +--+--+
                           |
                           |
                       +---+---+
                   +---+  SW2  +----+
                   |   +-------+    |
                   |                |
                   |                |
+-----+        +---+---+        +---+---+        +-----+
| PC1 +--------+  SW1  +--------+  SW3  +--------+ PC3 |
+-----+        +-------+        +-------+        +-----+
```

#### Plan d'adressage

Machine | `net1`
--- | ---
`PC1` | `10.2.2.1/24`
`PC2` | `10.2.2.2/24`
`PC3` | `10.2.2.3/24`

#### ToDo

* 🌞 mettre en place la topologie ci-dessus

![Topologie II](./images/TopologieII.png)


* 🌞 faire communiquer les trois PCs
  * avec des `ping` qui fonctionnent


  *Ping PC1 -> PC2:*

  ```bash
  PC-3> ping 10.2.2.2
  84 bytes from 10.2.2.2 icmp_seq=1 ttl=64 time=1.952 ms
  84 bytes from 10.2.2.2 icmp_seq=2 ttl=64 time=0.000 ms
  ```

  *Ping PC1 -> PC3:*
  
  ```bash
  PC-3> ping 10.2.2.3
  84 bytes from 10.2.2.3 icmp_seq=1 ttl=64 time=58.562 ms
  84 bytes from 10.2.2.3 icmp_seq=2 ttl=64 time=0.946 ms
  ```

  *Ping PC2 -> PC1:*

  ```bash
  PC-4> ping 10.2.2.1
  84 bytes from 10.2.2.1 icmp_seq=1 ttl=64 time=3.868 ms
  84 bytes from 10.2.2.1 icmp_seq=2 ttl=64 time=0.943 ms
  ```

  *Ping PC2 -> PC3:*
  
  ```bash
  PC-4> ping 10.2.2.3
  84 bytes from 10.2.2.3 icmp_seq=1 ttl=64 time=0.972 ms
  84 bytes from 10.2.2.3 icmp_seq=2 ttl=64 time=0.972 ms
  ```

  *Ping PC3 -> PC1:*

  ```bash
  PC-5> ping 10.2.2.1
  84 bytes from 10.2.2.1 icmp_seq=1 ttl=64 time=4.880 ms
  84 bytes from 10.2.2.1 icmp_seq=2 ttl=64 time=0.944 ms
  ```

  *Ping PC3 -> PC2:*
  
  ```bash
  PC-5> ping 10.2.2.2
  84 bytes from 10.2.2.2 icmp_seq=1 ttl=64 time=0.977 ms
  84 bytes from 10.2.2.2 icmp_seq=2 ttl=64 time=0.976 ms
  ```


* 🌞 analyser la table MAC d'un switch
  * `show mac address-table`

  *SW3*

  ```bash
    Vlan    Mac Address       Type        Ports
  ----    -----------       --------    -----
    1    0050.7966.6800    DYNAMIC     Et0/2
    1    0050.7966.6801    DYNAMIC     Et0/2
    1    0050.7966.6802    DYNAMIC     Et0/0
    1    aabb.cc00.0220    DYNAMIC     Et0/2
    1    aabb.cc00.0310    DYNAMIC     Et0/2
    1    aabb.cc00.0430    DYNAMIC     Et0/2
  ```

  * comprendre/expliquer chaque ligne

    Chaque ligne représente les adresses mac des machines avec lesquelles il a communiqué et par quel port il est passé pour cette communication.
    Les 3 premières viennent des PC, les 3 autres des switchs

* 🐙 en lançant Wireshark sur les liens des switches, il y a des trames CDP qui circulent. Quoi qu'est-ce ?

#### Mise en évidence du Spanning Tree Protocol

STP a été ici automatiquement configuré par les switches eux-mêmes pour éviter une boucle réseau. 

Dans une configuration pareille, les switches ont élu un chemin de préférence.  
Si on considère les trois liens qui unissent les switches :
* `SW1` <> `SW2`
* `SW2` <> `SW3`
* `SW1` <> `SW3`  

**L'un de ces liens a forcément été désactivé.**

On va regarder comment STP a été configuré.

* 🌞 déterminer les informations STP
  * à l'aide des [commandes dédiées au protocole](/memo/cli-cisco.md#stp)

    *Pour SW1*

    ```bash
    IOU2#show spanning-tree

    VLAN0001
      Spanning tree enabled protocol rstp
      Root ID    Priority    32769
                Address     aabb.cc00.0200
                This bridge is the root
                Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec

      Bridge ID  Priority    32769  (priority 32768 sys-id-ext 1)
                Address     aabb.cc00.0200
                Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec
                Aging Time  300 sec

    Interface           Role Sts Cost      Prio.Nbr Type
    ------------------- ---- --- --------- -------- --------------------------------
    Et0/0               Desg FWD 100       128.1    Shr
    Et0/1               Desg FWD 100       128.2    Shr
    Et0/2               Desg FWD 100       128.3    Shr
    Et0/3               Desg FWD 100       128.4    Shr
    Et1/0               Desg FWD 100       128.5    Shr
    Et1/1               Desg FWD 100       128.6    Shr
    Et1/2               Desg FWD 100       128.7    Shr
    Et1/3               Desg FWD 100       128.8    Shr
    ```

     *Pour SW2*

    ```bash
    IOU3#show spanning-tree

    VLAN0001
      Spanning tree enabled protocol rstp
      Root ID    Priority    32769
                Address     aabb.cc00.0200
                Cost        100
                Port        2 (Ethernet0/1)
                Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec

      Bridge ID  Priority    32769  (priority 32768 sys-id-ext 1)
                Address     aabb.cc00.0300
                Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec
                Aging Time  300 sec

    Interface           Role Sts Cost      Prio.Nbr Type
    ------------------- ---- --- --------- -------- --------------------------------
    Et0/0               Desg FWD 100       128.1    Shr
    Et0/1               Root FWD 100       128.2    Shr
    Et0/2               Desg FWD 100       128.3    Shr
    Et0/3               Desg FWD 100       128.4    Shr
    Et1/0               Desg FWD 100       128.5    Shr
    Et1/1               Desg FWD 100       128.6    Shr
    Et1/2               Desg FWD 100       128.7    Shr
    ```

     *Pour SW3*

    ```bash
    IOU4#show spanning-tree

    VLAN0001
      Spanning tree enabled protocol rstp
      Root ID    Priority    32769
                Address     aabb.cc00.0200
                Cost        100
                Port        3 (Ethernet0/2)
                Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec

      Bridge ID  Priority    32769  (priority 32768 sys-id-ext 1)
                Address     aabb.cc00.0400
                Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec
                Aging Time  300 sec

    Interface           Role Sts Cost      Prio.Nbr Type
    ------------------- ---- --- --------- -------- --------------------------------
    Et0/0               Desg FWD 100       128.1    Shr
    Et0/1               Desg FWD 100       128.2    Shr
    Et0/2               Root FWD 100       128.3    Shr
    Et0/3               Altn BLK 100       128.4    Shr
    Et1/0               Desg FWD 100       128.5    Shr
    Et1/1               Desg FWD 100       128.6    Shr
    Et1/2               Desg FWD 100       128.7    Shr
    ```

* 🌞 faire un schéma en représentant les informations STP
  * rôle des switches (qui est le root bridge)
  * rôle de chacun des ports

  ```

                          +-----+
                          | PC2 |
                          +--+--+
          Ce port est root  |
                      |     |
                      v +---+---+
                    +---+  SW2  +----+
                    |   +-------+    |
                    |                |
                    |                | <---- Ce port est bloqué
  +-----+        +---+---+        +---+---+        +-----+
  | PC1 +--------+  SW1  +--------+  SW3  +--------+ PC3 |
  +-----+        +-------+        +-------+        +-----+
                     ^           ^
                     |           |
                Root Bridge      |
                                 |
                          Ce port est root

```

* 🌞 confirmer les informations STP
  * effectuer un `ping` d'une machine à une autre
  * vérifier que les trames passent bien par le chemin attendu (Wireshark)
* 🌞 ainsi, déterminer quel lien a été désactivé par STP
* 🌞 faire un schéma qui explique le trajet d'une requête ARP lorsque PC1 ping PC3, et de sa réponse
  * représenter **TOUTES** les trames ARP (n'oubliez pas les broadcasts)

#### Reconfigurer STP

* 🌞 changer la priorité d'un switch qui n'est pas le *root bridge*
* 🌞 vérifier les changements
  * avec des commandes sur les switches

# III. Isolation

## 1. Simple
 
#### Topologie
```
  +-----+        +-------+        +-----+
  | PC1 +--------+  SW1  +--------+ PC3 |
  +-----+      10+-------+20      +-----+
                  20|
                    |
                  +--+--+
                  | PC2 |
                  +-----+
```

#### Plan d'adressage

Machine | IP `net1` | VLAN
--- | --- | --- 
`PC1` | `10.2.3.1/24` | 10
`PC2` | `10.2.3.2/24` | 20
`PC3` | `10.2.3.3/24` | 20

#### ToDo

* 🌞 mettre en place la topologie ci-dessus
  * voir [les commandes dédiées à la manipulation de VLANs](/memo/cli-cisco.md#vlan)
* 🌞 faire communiquer les PCs deux à deux
  * vérifier que `PC2` ne peut joindre que `PC3`
  * vérifier que `PC1` ne peut joindre personne alors qu'il est dans le même réseau (sad)

## 2. Avec trunk

#### Topologie

```
+-----+        +-------+        +-------+        +-----+
| PC1 +--------+  SW1  +--------+  SW2  +--------+ PC4 |
+-----+      10+-------+        +-------+20      +-----+
                 20|              10|
                   |                |
                +--+--+          +--+--+
                | PC2 |          | PC3 |
                +-----+          +-----+
```

#### Plan d'adressage

Machine | IP `net1` | IP `net2` | VLAN
--- | --- | --- | ---
`PC1` | `10.2.10.1/24` | X | 10
`PC2` | X | `10.2.20.1/24` | 20
`PC3` | `10.2.10.2/24` | X | 10
`PC4` | X | `10.2.20.2/24` | 20

#### ToDo

* 🌞 mettre en place la topologie ci-dessus
* 🌞 faire communiquer les PCs deux à deux
  * vérifier que `PC1` ne peut joindre que `PC3`
  * vérifier que `PC4` ne peut joindre que `PC2`
* 🌞 mettre en évidence l'utilisation des VLANs avec Wireshark

# IV. Need perfs

#### Topologie

Pareil qu'en [III.2.](#2-avec-trunk) à part le lien entre SW1 et SW2 qui est doublé.

```
+-----+        +-------+--------+-------+        +-----+
| PC1 +--------+  SW1  |        |  SW2  +--------+ PC4 |
+-----+      10+-------+--------+-------+20      +-----+
                 20|              10|
                   |                |
                +--+--+          +--+--+
                | PC2 |          | PC3 |
                +-----+          +-----+

```
#### Plan d'adressage

Pareil qu'en [III.2.](#2-avec-trunk).

Machine | IP `net1` | IP `net2` | VLAN
--- | --- | --- | ---
`PC1` | `10.2.10.1/24` | X | 10
`PC2` | X | `10.2.20.1/24` | 20
`PC3` | `10.2.10.2/24` | X | 10
`PC4` | X | `10.2.20.2/24` | 20

#### ToDo

* 🌞 mettre en place la topologie ci-dessus
  * configurer LACP entre `SW1` et `SW2`
  * utiliser Wireshark pour mettre en évidence l'utilisation de trames LACP
  * **vérifier avec un `show ip interface po1` que la bande passante a bien été doublée**

> Pas de failover possible sur les IOUs malheureusement :( (voir [ce doc](https://www.cisco.com/c/en/us/td/docs/switches/blades/3020/software/release/12-2_58_se/configuration/guide/3020_scg/swethchl.pdf), dernière section. Pas de link state dans les IOUs)
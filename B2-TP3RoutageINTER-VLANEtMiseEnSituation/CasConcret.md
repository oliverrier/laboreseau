# Cas Concret

## Adressage IP

Chaque rôle est dans un réseau avec le même VLan.

### Salle R4

| Nom | Rôles | Net | IP | Vlan | NetMask
| ---- | ---- | ---- | ---- | ---- | ----
| Admin 1 | Admin | 192.168.10.0 | 192.168.10.1 | 10 | /29
| User 7 | User | 192.168.20.0 | 192.168.20.1 | 20 | /27
| User 8 | User | 192.168.20.0 | 192.168.20.2 | 20 | /27
| User 9 | User | 192.168.20.0 | 192.168.20.3 | 20 | /27
| User 10 | User | 192.168.20.0 | 192.168.20.4 | 20 | /27
| Stagiaire 3 | Stagiaire | 192.168.30.0 | 192.168.30.1 | 30 | /28
| Stagiaire 4 | Stagiaire | 192.168.30.0 | 192.168.30.2 | 30 | /28
| Stagiaire 5 | Stagiaire | 192.168.30.0 | 192.168.30.3 | 30 | /28
| Printer 1 | Imprimante | 192.168.50.0 | 192.168.50.1 | 50 | /28


### Salle R2

| Nom | Rôles | Net | IP | Vlan | NetMask
| ---- | ---- | ---- | ---- | ---- | ----
| Serveur 1 | Serveur | 192.168.45.0 | 192.168.45.1 | 45 | /28
| Serveur 2 | Serveur | 192.168.40.0 | 192.168.40.2 | 40 | /28
| Serveur 3 | Serveur | 192.168.40.0 | 192.168.40.3 | 40 | /28
| Serveur 4 | Serveur | 192.168.40.0 | 192.168.40.4 | 40 | /28
| Serveur 5 | Serveur | 192.168.40.0 | 192.168.40.5 | 40 | /28
| Serveur 6 | Serveur | 192.168.45.0 | 192.168.45.6 | 45 | /28

